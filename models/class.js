const mongoose = require('mongoose');
const { Schema } = mongoose;

const ClassSchema = new Schema({
    title: String,
    videos: [
        {
            type: String
        }
    ],
    // videoId: String,
    supportMaterial: [
        {
            type: String
        }
    ],
    inClassExercises: [
        {
            type: String
        }
    ],
    outOfClassExercises: [
        {
            type: String
        }
    ]
});

module.exports = mongoose.model('Class', ClassSchema);