const mongoose = require('mongoose');
const { Schema } = mongoose;
const Class = require('./class');

const opts = { toJSON: { virtuals: true } };

const CourseDashboardSchema = new Schema({
    title: String,
    playlistId: {
        type: String,
        required: true
    },
    environmentUrl: String,
    forumUrl: String,
    videos: [
        {
            title: String,
            videoId: String,
            thumbnail: String
        }
    ],
    classes: [
        {
            title: String,
            supportMaterial: [
                {
                    type: String
                }
            ],
            inClassExercises: [
                {
                    type: String
                }
            ],
            outOfClassExercises: [
                {
                    type: String
                }
            ],
            videos: [
                {
                    videoId: String
                }
            ]
        }
    ],
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, opts);

CourseDashboardSchema.post('findOneAndDelete', async doc => {
    if (doc) {
        await Class.remove({ _id: { $in: doc.classes } });
    }
});

module.exports = mongoose.model('CourseDashboard', CourseDashboardSchema);